// Dice Rolling
 
import java.util.Random;
 
public class DiceRolling
{
    public static void main( String[] args)
    {
        Random randomNumbers = new Random(); // Generates random numbers
        int[] array = new int[ 7 ]; // Declares the array
         
        //Roll the die 36,000 times
        for ( int roll = 1; roll <=1000; roll++ )
            ++array[ 1 + randomNumbers.nextInt ( 6 ) ];
         
        System.out.printf( "%s%10s%12s\n", "Face", "Frequency", "Percentage" );
         
        // outputs array values
        for ( int face = 1; face < array.length; face++ )
            System.out.printf( "%4d%10d%10d\n", face, array[ face ], array[ face ] );
         
    } // end main
     
} // end class DiceRolling